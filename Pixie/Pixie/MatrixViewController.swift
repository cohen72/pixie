//
//  MatrixViewController.swift
//  Pixie
//
//  Created by Mr. Cohen on 16/11/2016.
//  Copyright © 2016 Mr. Cohen. All rights reserved.
//

import UIKit

class MatrixViewController: UIViewController, GridViewDelegate  {

    @IBOutlet weak var gridView: GridView!
    @IBOutlet weak var lblStatus: UILabel!
    
    var matrix: Matrix = Matrix.init(rows: 0, columns: 0, percentage: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        gridView.delegate = self
        gridView.setupRandom(matrix: matrix)
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(reset))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func calculate(_ sender: Any) {
        gridView.calculateIslands()
    }
    
    func reset(_ sender: Any) {
        gridView.resetIslands()
    }
    
    func didChangeState(sender: GridView) {
        switch sender.gridState {
        case .initial, .reset:
            lblStatus.text = ""
            break
        case .marking:
            lblStatus.text = "You've got " + String(sender.numOfIslands) + " Islands Chillin' here"
            break
        }
    }
    
    func didBeginCalculating(sender: GridView) {
        lblStatus.text = "Hmmmmm ..."
    }
    
}
