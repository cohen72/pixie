//
//  RootViewController
//  Pixie
//
//  Created by Mr. Cohen on 16/11/2016.
//  Copyright © 2016 Mr. Cohen. All rights reserved.
//

import UIKit


extension NSRange {
    
    func range(for str: String) -> Range<String.Index>? {
        guard location != NSNotFound else { return nil }
        
        guard let fromUTFIndex = str.utf16.index(str.utf16.startIndex, offsetBy: location, limitedBy: str.utf16.endIndex) else { return nil }
        guard let toUTFIndex = str.utf16.index(fromUTFIndex, offsetBy: length, limitedBy: str.utf16.endIndex) else { return nil }
        guard let fromIndex = String.Index(fromUTFIndex, within: str) else { return nil }
        guard let toIndex = String.Index(toUTFIndex, within: str) else { return nil }
        
        return fromIndex ..< toIndex
    }
}

class RootViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var btnGo: UIButton!
    
    @IBOutlet weak var txtM: UITextField!
    @IBOutlet weak var txtN: UITextField!
    @IBOutlet weak var sliderPercentage: UISlider!
    @IBOutlet weak var lblIslandPercentage: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtM.addTarget(self, action: #selector(textFieldDidChange), for: UIControlEvents.editingChanged)
        txtN.addTarget(self, action: #selector(textFieldDidChange), for: UIControlEvents.editingChanged)
        txtM.delegate = self
        txtN.delegate = self
        btnGo.isEnabled = false
        sliderValueChanged(sliderPercentage)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func sliderValueChanged(_ sender: Any) {
        lblIslandPercentage.text = String(Int(sliderPercentage.value)) + "0%"
    }
    
    func textFieldDidChange() {
        if (txtN.text?.isEmpty)! && (txtM.text?.isEmpty)! {
            btnGo.isEnabled = false
        } else {
            btnGo.isEnabled = true
        }
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        guard let stringRange = range.range(for: currentText) else { return false }
        
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)

        // Keep grid less than 300 * 300
        if updatedText.characters.count > 0 {
            if Int(updatedText)! > 300 {
                return false
            }
        }
        
        return updatedText.characters.count <= 3
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let identifier = segue.identifier {
            
            if identifier == "showGeneratedRandomArray" {
                let matrixVc = segue.destination as! MatrixViewController
                matrixVc.matrix = Matrix.init(rows: Int(txtM.text!)!, columns: Int(txtN.text!)!, percentage: Double(sliderPercentage.value))
            }
        }        
    }
    
}

