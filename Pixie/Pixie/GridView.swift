//
//  GridView.swift
//  Pixie
//
//  Created by Mr. Cohen on 16/11/2016.
//  Copyright © 2016 Mr. Cohen. All rights reserved.
//

import UIKit

enum GridState {
    case initial
    case marking
    case reset
}

protocol GridViewDelegate {
    func didChangeState(sender: GridView)
    func didBeginCalculating(sender: GridView)
}


class GridView: UIView {
    
    private var matrix: Matrix = Matrix.init(rows: 0, columns: 0, percentage: 0)
    
    var pixelWidth: CGFloat = 0.0
    var pixelHeight: CGFloat = 0.0
    var gridState: GridState = GridState.initial
    var numOfIslands = 0
    var delegate: GridViewDelegate?
    
    func setupRandom(matrix: Matrix) {
        self.matrix = matrix
        self.gridState = .initial
        self.setNeedsLayout()
    }
    
    override func draw(_ rect: CGRect) {

        super.draw(rect)
        
        pixelWidth = rect.size.width / CGFloat(matrix.columns)
        pixelHeight = rect.size.height / CGFloat(matrix.rows)
        
        let context = UIGraphicsGetCurrentContext()!
        
        for row in 0..<matrix.rows {
            
            for column in 0..<matrix.columns {
                
                context.setStrokeColor(UIColor.lightGray.cgColor)
                context.setLineWidth(0.1)
                
                let frame = CGRect.init(x:CGFloat(column) * pixelWidth, y:CGFloat(row) * pixelHeight, width:pixelWidth, height:pixelHeight)
                context.stroke(frame)
                
                switch gridState {
                case .initial:
                    if matrix.indexIsBlack(row, column: column) {
                        drawPixel(row: row, column: column, color: UIColor.black)
                    }
                    break
                case .reset:
                    if matrix.indexIsMarked(row, column: column) {
                        matrix[row, column] = 1.0
                        drawPixel(row: row, column: column, color: UIColor.black)
                    }
                    break
                case .marking:
                    if matrix.indexIsBlack(row, column: column) {
                        markNeighbors(r: row, c: column, color: UIColor.getRandomColor())
                        numOfIslands += 1
                    }
                    break
                }
            }
        }

        delegate?.didChangeState(sender: self)

    }
    
    func markNeighbors(r: Int, c:Int, color: UIColor) {
        
        // mark the coordinate as found
        
        matrix[r, c] = 2.0
        
        drawPixel(row:r, column:c, color:color)
        
        // recursively mark all found neighbors and their neighbors and their neighbors and ... 
        
        for direction in Direction.allValues {
            
            if let neighbor = matrix.getNeighbor(coordinate: (r,c), direction: direction) {
                markNeighbors(r: neighbor.0, c: neighbor.1, color: color)
            }
        }
    }
    
    func drawPixel(row: Int, column: Int, color:UIColor) {
        let context = UIGraphicsGetCurrentContext()!
        
        context.setFillColor(color.cgColor)
        
        let width = bounds.size.width / CGFloat(matrix.columns)
        let height = bounds.size.height / CGFloat(matrix.rows)
        let x = CGFloat(column) * width
        let y = CGFloat(row) * height
        
        let frame = CGRect(x:x, y:y, width:width, height:height)
        context.fill(frame)
    }
    
    func calculateIslands(){
        guard gridState != .marking else {
            return
        }
        self.numOfIslands = 0
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            self.delegate?.didBeginCalculating(sender: self)
            DispatchQueue.main.asyncAfter(deadline: .now()) {
                self.gridState = .marking
                self.setNeedsDisplay()
            }
        }
    }
    
    func resetIslands() {
        if gridState == .marking {
            gridState = .reset
            setNeedsDisplay()
        }
    }
    
}

