//
//  UIColor+random.swift
//  Pixie
//
//  Created by Mr. Cohen on 17/11/2016.
//  Copyright © 2016 Mr. Cohen. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    static func getRandomColor() -> UIColor {
        let randomRed:CGFloat = CGFloat(drand48())
        let randomGreen:CGFloat = CGFloat(drand48())
        let randomBlue:CGFloat = CGFloat(drand48())
        return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
    }
}
