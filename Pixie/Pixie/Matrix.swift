//
//  Matrix.swift
//  Pixie
//
//  Created by Mr. Cohen on 16/11/2016.
//  Copyright © 2016 Mr. Cohen. All rights reserved.
//

import Foundation


enum Direction: String {
    case tl = "topLeft"
    case tt = "top"
    case tr = "topRight"
    case ll = "left"
    case rr = "right"
    case bl = "bottomLeft"
    case bb = "bottom"
    case br = "bottomRight"
    
    static let allValues = [tl,tt,tr,ll,rr,bl,bb,br]
}


struct Matrix {
    
    let rows: Int, columns: Int
    var grid: [Double] = [Double]()
    
    init(rows: Int, columns: Int, percentage: Double) {
        self.rows = rows
        self.columns = columns
        grid = generateRandomArray(n: rows * columns, percentage: percentage)
    }
    
    func generateRandomArray(n:Int, percentage: Double ) -> [Double] {
        var result:[Double] = []
        for _ in 0..<n {
            let randomNumber = Double(arc4random_uniform(11) + 0)
            result.append(randomNumber < percentage ? 1.0 : 0.0)
        }
        return result
    }

    
    func indexIsValid(_ row: Int, column: Int) -> Bool {
        return row >= 0 && row < rows && column >= 0 && column < columns
    }
    subscript(row: Int, column: Int) -> Double {
        get {
            assert(indexIsValid(row, column: column), "Index out of range")
            return grid[(row * columns) + column]
        }
        set {
            assert(indexIsValid(row, column: column), "Index out of range")
            grid[(row * columns) + column] = newValue
        }
    }
    
    func indexIsBlack(_ row: Int, column: Int) -> Bool {
        if self[row, column] == 1.0 {
            return true
        }
        return false
    }
    
    func indexIsMarked(_ row: Int, column: Int) -> Bool {
        if self[row, column] == 2.0 {
            return true
        }
        return false
    }
        
    func getNeighbor(coordinate:(Int, Int), direction: Direction) -> (Int, Int)? {
        
        var neighborRow = coordinate.0
        var neighborColumn = coordinate.1
        
        switch direction {
            case .tt:
                neighborRow -= 1
                break
            case .tl:
                neighborRow -= 1
                neighborColumn -= 1
                break
            case .tr:
                neighborRow -= 1
                neighborColumn += 1
                break
            case .ll:
                neighborColumn -= 1
                break
            case .rr:
                neighborColumn += 1
                break
            case .bb:
                neighborRow += 1
                break
            case .bl:
                neighborRow += 1
                neighborColumn -= 1
                break
            case .br:
                neighborRow += 1
                neighborColumn += 1
                break
            
        }
        
        guard indexIsValid(neighborRow, column: neighborColumn) else {
            return nil
        }
        
        guard indexIsBlack(neighborRow, column: neighborColumn) else {
            return nil
        }
        
        return (neighborRow, neighborColumn)        
    }
}
